# Scan Execution Policy Project

The Scan Execution Policy Project is a repository used to store security policies. These policies can be imported by all other projects in GitLab. All security policies are stored as a YAML file named `.gitlab/security-policies/policy.yml`, in this format:

```yaml
---
scan_execution_policy:
- name: Policy name (depicts what the policy does)
  description: Gives a brief description on the actions performed by the policy
  enabled: true
  rules:
  - type: <pipeline/scheduled>
    branches:
    - master
    - main
  actions:
  - scan: <scan-name>
    tags:
    - <tag-name>
```

You can read more about the format and policies schema in the [documentation](https://docs.gitlab.com/ee/user/application_security/policies/#scan-execution-policies-schema).

## Default branch protection settings

This project is preconfigured with the default branch set as a protected branch, and only maintainers/owners of [CERN-SecurityPolicies](https://gitlab.cern.ch/cern-securitypolicies) have permission to merge into that branch.
